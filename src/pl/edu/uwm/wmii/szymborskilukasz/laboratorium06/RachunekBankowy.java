package pl.edu.uwm.wmii.szymborskilukasz.laboratorium06;

class RachunekBankowy {
    double saldo;

    double obliczMiesieczneOdsetki(double saldo, double rocznaStopaProcentowa) {
        return saldo+((saldo*rocznaStopaProcentowa)/12);
    }

    void setRocznaStopaProcentowa(double rocznaStopaProcentowa, double nowaRocznaStopaProcentowa) {
        rocznaStopaProcentowa = nowaRocznaStopaProcentowa;
    }
}
