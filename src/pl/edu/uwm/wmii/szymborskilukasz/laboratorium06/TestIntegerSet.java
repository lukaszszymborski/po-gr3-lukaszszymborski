package pl.edu.uwm.wmii.szymborskilukasz.laboratorium06;

public class TestIntegerSet {

    static public void main(String args[]) {
        IntegerSet set1 = new IntegerSet(10);
        set1.insertElement(4);
        set1.insertElement(5);
        set1.insertElement(1);
        String output1 = set1.toString();
        System.out.println(output1);
        set1.deleteElement(4);
        output1 = set1.toString();
        System.out.println(output1);
        IntegerSet set2 = new IntegerSet(10);
        set2.insertElement(8);
        set2.insertElement(3);
        String output2 = set2.toString();
        System.out.println(output2);
        IntegerSet setU = IntegerSet.union(set1, set2);
        String outputU = setU.toString();
        System.out.println(outputU);
    }

}
