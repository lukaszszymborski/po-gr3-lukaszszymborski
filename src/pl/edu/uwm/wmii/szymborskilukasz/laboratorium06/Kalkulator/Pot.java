package pl.edu.uwm.wmii.szymborskilukasz.laboratorium06.Kalkulator;

class Pot {

    float potegowanie(double liczba1, double liczba2) {
        float wynik = (float)liczba1;
        if(liczba2==0) {
            return 1;
        }
        else {
            for (int i = 1; i < liczba2; i++) {
                wynik *= liczba1;
            }
            return wynik;
        }
    }

}
