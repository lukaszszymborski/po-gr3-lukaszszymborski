package pl.edu.uwm.wmii.szymborskilukasz.laboratorium06.Kalkulator;

class Dziel {

    static float dzielenie(double liczba1, double liczba2) {
        if(liczba2 == 0) {
            return Float.NaN;
        }
        else {
            return (float) (liczba1 / liczba2);
        }
    }

}
