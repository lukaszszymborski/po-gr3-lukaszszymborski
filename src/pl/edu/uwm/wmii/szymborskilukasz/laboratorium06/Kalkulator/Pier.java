package pl.edu.uwm.wmii.szymborskilukasz.laboratorium06.Kalkulator;

class Pier {

    static float pierwiastek(double liczba1) {
        if (liczba1 == 0 || liczba1 == 1) {
            return (float)liczba1;
        }
        float x1 = (float)liczba1;
        float x2;
        for (int i = 0; i < liczba1; i++) {
            x2 = x1 - (x1 * x1 - (float)liczba1) / (2 * x1);
            var delta = (x2 - x1) / x1;
            if (delta < .000001 && delta > -.000001) {
                return x2;
            }
            x1 = x2;
        }
        return x1;
    }
}
