package pl.edu.uwm.wmii.szymborskilukasz.laboratorium06.Kalkulator;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Test {

    public static void main(String args[]) {
        ArrayList<Float> wyniki = new ArrayList<Float>();
        try {
            System.out.print("Podaj liczbę 1: ");
            Scanner tempLiczba1 = new Scanner(System.in);
            double liczba1 = tempLiczba1.nextDouble();
            System.out.print("Podaj liczbę 2: ");
            Scanner tempLiczba2 = new Scanner(System.in);
            double liczba2 = tempLiczba2.nextDouble();
            float wynik1 = Dodaj.dodawanie(liczba1, liczba2);
            wyniki.add(wynik1);
            Odejmij roznica = new Odejmij();
            float wynik2 = roznica.odejmowanie(liczba1, liczba2);
            wyniki.add(wynik2);
            float wynik3 = Dziel.dzielenie(liczba1, liczba2);
            wyniki.add(wynik3);
            Mnoz iloczyn = new Mnoz();
            float wynik4 = iloczyn.mnozenie(liczba1, liczba2);
            wyniki.add(wynik4);
            float wynik5 = Pier.pierwiastek(liczba1);
            wyniki.add(wynik5);
            Pot potega = new Pot();
            float wynik6 = potega.potegowanie(liczba1, liczba2);
            wyniki.add(wynik6);
            for(int i=0;i<wyniki.size();i++) {
                System.out.print(wyniki.get(i)+", ");
            }
        }
        catch (InputMismatchException|IllegalArgumentException e) {
            System.out.println("Nie można przechwycić liczby.");
        }
    }

}
