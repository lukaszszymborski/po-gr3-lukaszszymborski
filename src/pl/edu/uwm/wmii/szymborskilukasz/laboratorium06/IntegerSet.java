package pl.edu.uwm.wmii.szymborskilukasz.laboratorium06;

public class IntegerSet {
    static int liczba;
    boolean[] tab;
    IntegerSet set;

    public IntegerSet(int liczba) {
        this.liczba = liczba;
        tab = new boolean[liczba];
    }

    public boolean getElement(int i) {
        return tab[i];
    }

    void insertElement(int k) {
        tab[k] = true;
    }

    void deleteElement(int l) {
        tab[l] = false;
    }

    static public IntegerSet union(IntegerSet set1, IntegerSet set2) {
        IntegerSet merged = new IntegerSet(liczba);
        for(int i=0;i<liczba;i++) {
            if(set1.getElement(i) || set2.getElement(i)) {
                merged.insertElement(i);
            }
        }
        return merged;
    }

    static public IntegerSet intersection(IntegerSet set1, IntegerSet set2) {
        IntegerSet merged = new IntegerSet(liczba);
        for(int i=0;i<liczba;i++) {
            if(set1.getElement(i) && set2.getElement(i)) {
                merged.insertElement(i);
            }
        }
        return merged;
    }

    public String toString() {
        int val = getElement(0)? 1 : 0;
        String output = String.valueOf(val);
        for(int i=1;i<liczba;i++) {
            val = getElement(i) ? 1 : 0;
            output += " ";
            output += String.valueOf(val);
        }
        return output;
    }
}
