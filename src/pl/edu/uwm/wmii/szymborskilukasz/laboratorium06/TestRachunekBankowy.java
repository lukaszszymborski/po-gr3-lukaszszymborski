package pl.edu.uwm.wmii.szymborskilukasz.laboratorium06;

public class TestRachunekBankowy {

    public static void main(String args[]) {
        double RocznaStopaProcentowa = 0.04;
        RachunekBankowy saver1 = new RachunekBankowy();
        saver1.saldo = 2000;
        RachunekBankowy saver2 = new RachunekBankowy();
        saver2.saldo = 3000;
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
        saver1.saldo = saver1.obliczMiesieczneOdsetki(saver1.saldo, RocznaStopaProcentowa);
        saver2.saldo = saver2.obliczMiesieczneOdsetki(saver2.saldo, RocznaStopaProcentowa);
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
        RocznaStopaProcentowa = 0.05;
        saver1.saldo = saver1.obliczMiesieczneOdsetki(saver1.saldo, RocznaStopaProcentowa);
        saver2.saldo = saver2.obliczMiesieczneOdsetki(saver2.saldo, RocznaStopaProcentowa);
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
    }
}
