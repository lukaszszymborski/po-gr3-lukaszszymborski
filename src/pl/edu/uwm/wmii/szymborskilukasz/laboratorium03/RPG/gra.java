package pl.edu.uwm.wmii.szymborskilukasz.laboratorium03.RPG;

import java.util.Random;

public class gra {

    public static void main(String args[]) {
        lucznik Legolas = new lucznik("Legolas", 100, 3, 15);
        wojownik Aragorn = new wojownik("Aragorn", 100, 1, 15);
        potwor Troll = new potwor();
        System.out.println(Legolas.toString());
        System.out.println(Aragorn.toString());
        while(Legolas.zdrowie>0 || Aragorn.zdrowie>0 || Troll.zdrowie>0) {
            System.out.println("Zdrowie potwora: "+Troll.zdrowie+" \tZdrowie Legolasa: "+Legolas.zdrowie+" \tZdrowie Aragorna: "+Aragorn.zdrowie);
            Random rnd = new Random();
            int celT = rnd.nextInt(1);
            if(celT==0) {
                Troll.zmianaZdrowia(Troll.zdrowie, (int) Legolas.atak);
            }
            else {
                Troll.zmianaZdrowia(Troll.zdrowie, (int) Aragorn.atak);
            }
            int cel = rnd.nextInt(1);
            if(cel==0) {
                Legolas.zmianaZdrowia(Legolas.zdrowie, Troll.atak);
            }
            else {
                Aragorn.zmianaZdrowia(Aragorn.zdrowie, Troll.atak);
            }
        }
    }
}
