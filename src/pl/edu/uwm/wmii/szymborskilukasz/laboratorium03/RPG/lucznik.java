package pl.edu.uwm.wmii.szymborskilukasz.laboratorium03.RPG;

class lucznik extends bohater{
    int zrecznosc;
    double atak = atak(zrecznosc, taktyka, zdrowie);

    private double atak(int zrecznosc, int taktyka, int zdrowie) {
        int pAtaku = zrecznosc*taktyka*zdrowie;
        return pAtaku;
    }

    public lucznik() {
        this.imie = "GoblinA";
        this.zdrowie = 100;
        this.taktyka = 3;
        this.zrecznosc = 15;
    }

    lucznik(String imie, int zdrowie, int taktyka, int zrecznosc) {
        this.imie = imie;
        this.zdrowie = zdrowie;
        this.taktyka = taktyka;
        this.zrecznosc = zrecznosc;
        this.atak = atak(zrecznosc, taktyka, zdrowie);
    }

    public String toString() {
        return "imie: "+imie+" \tzdrowie: "+zdrowie+" \tPT: "+taktyka+" \tzrecznosc: "+zrecznosc+" \tatak: "+atak/100;
    }

    void zmianaZdrowia(int zdrowie, int atak) {
        zdrowie -= atak/2;
    }
}
