package pl.edu.uwm.wmii.szymborskilukasz.laboratorium03.RPG;

class wojownik extends bohater {
    int sila;
    double atak = atak(sila, taktyka, zdrowie);

    private double atak(int sila, int taktyka, int zdrowie) {
        int pAtaku = sila*taktyka*zdrowie;
        return pAtaku;
    }

    public wojownik() {
        this.imie = "OrkA";
        this.zdrowie = 100;
        this.taktyka = 1;
        this.sila = 15;
    }

    wojownik(String imie, int zdrowie, int taktyka, int zrecznosc) {
        this.imie = imie;
        this.zdrowie = zdrowie;
        this.taktyka = taktyka;
        this.sila = zrecznosc;
        this.atak = atak(zrecznosc, taktyka, zdrowie);
    }

    public String toString() {
        return "imie: "+imie+" \tzdrowie: "+zdrowie+" \tPT: "+taktyka+" \tsila: "+sila+" \tatak: "+atak/100;
    }

    void zmianaZdrowia(int zdrowie, int atak) {
        zdrowie -= atak/4;
    }
}
