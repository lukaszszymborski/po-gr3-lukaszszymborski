package pl.edu.uwm.wmii.szymborskilukasz.laboratorium05;

import java.util.ArrayList;

public class Zadanie4 {

    static public ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> rev = new ArrayList<Integer>();
        for(int i=a.size()-1;i>=0;i--) {
            rev.add(a.get(i));
        }
        return rev;
    }

    public static void main(String args[]) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(4);
        b.add(7);
        b.add(9);
        b.add(9);
        b.add(11);
        ArrayList<Integer> merged = reversed(a);
        System.out.print(merged);
    }
}
