package pl.edu.uwm.wmii.szymborskilukasz.laboratorium05;

import java.util.ArrayList;

public class Zadanie1 {

    static public ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> merged = new ArrayList<Integer>();
        for(int i=0;i<a.size();i++) { merged.add(a.get(i)); }
        for (int j=0;j<b.size();j++) { merged.add(b.get(j)); }
        return merged;
    }

    public static void main(String args[]) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        ArrayList<Integer> merged = append(a, b);
        System.out.print(merged);
    }
}
