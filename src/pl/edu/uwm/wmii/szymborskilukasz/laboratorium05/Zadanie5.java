package pl.edu.uwm.wmii.szymborskilukasz.laboratorium05;

import java.util.ArrayList;

public class Zadanie5 {

    static public void reverse(ArrayList<Integer> a) {
        if(a.size()%2==0) {
            int srodek = a.size()/2;
            for(int i=0;i<srodek;i++) {
                int temp = a.get(i);
                a.set(i, a.get(a.size()-1-i));
                a.set(a.size()-1-i, temp);
            }
        }
        else {
            double srodek = (double)a.size()/2;
            for(int i=0;i<srodek;i++) {
                int temp = a.get(i);
                a.set(i, a.get(a.size()-1-i));
                a.set(a.size()-1-i, temp);
            }
        }
    }

    public static void main(String args[]) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(4);
        b.add(7);
        b.add(9);
        b.add(9);
        b.add(11);
        System.out.print(b);
        reverse(b);
        System.out.print(b);
    }
}
