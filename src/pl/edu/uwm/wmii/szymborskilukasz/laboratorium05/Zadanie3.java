package pl.edu.uwm.wmii.szymborskilukasz.laboratorium05;

import java.util.ArrayList;

public class Zadanie3 {

    static public ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> merged = new ArrayList<Integer>();
        int i=0;
        int j=0;
        while(i<a.size()) {
            while (j < b.size()) {
                if(a.get(i)<b.get(j)) {
                    merged.add(a.get(i));
                    i++;
                    continue;
                }
                if(b.get(j)<a.get(i)) {
                    merged.add(b.get(j));
                    j++;
                    continue;
                }
                if(a.get(i).equals(b.get(j))) {
                    merged.add(a.get(i));
                    i++;
                    merged.add(b.get(j));
                    j++;
                    continue;
                }
            }
            if(j==b.size()) {
                merged.add(a.get(i));
                i++;
            }
        }

        return merged;
    }

    public static void main(String args[]) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(4);
        b.add(7);
        b.add(9);
        b.add(9);
        b.add(11);
        ArrayList<Integer> merged = mergeSorted(a, b);
        System.out.print(merged);
    }
}
