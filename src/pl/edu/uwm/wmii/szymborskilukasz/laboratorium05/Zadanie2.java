package pl.edu.uwm.wmii.szymborskilukasz.laboratorium05;

import java.util.ArrayList;

public class Zadanie2 {

    static public ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> merged = new ArrayList<Integer>();
        int x = 0;
        if(a.size()>b.size()) {
            for (int i = 0; i < b.size(); i++) {
                merged.add(a.get(i));
                merged.add(b.get(i));
                x = i;
            }
            for (int i = x+1;i<a.size();i++) {
                merged.add(a.get(i));
            }
        }
        else if(a.size()<b.size()) {
            for (int i = 0; i < a.size(); i++) {
                merged.add(a.get(i));
                merged.add(b.get(i));
                x = i;
            }
            for (int i = x+1;i<b.size();i++) {
                merged.add(b.get(i));
            }
        }
        else {
            for(int i=0;i<a.size();i++) {
                merged.add(a.get(i));
                merged.add(b.get(i));
            }
        }

        return merged;
    }

    public static void main(String args[]) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        ArrayList<Integer> merged = merge(a, b);
        System.out.print(merged);
    }
}
