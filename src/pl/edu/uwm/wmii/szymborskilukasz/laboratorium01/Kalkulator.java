package pl.edu.uwm.wmii.szymborskilukasz.laboratorium01;

public class Kalkulator {
    static int dodaj(int pierwsza, int druga) {
            return (pierwsza + druga);
        }
    static int odejmij(int pierwsza, int druga) {
            return (pierwsza - druga);
        }
    static int mnoz(int pierwsza, int druga) {
            return (pierwsza * druga);
        }
    static float dziel(int pierwsza, int druga) {
            return((float)pierwsza/druga);
        }
    static int mod(int pierwsza, int druga) {
            return(pierwsza%druga);
        }
    static double pierw(int pierwsza, int druga) {
            return (Math.pow((double) pierwsza, (double) druga));
        }
    public static void main(String[] args) {
        int a = 11;
        int b = 6;
        int c = dodaj(a, b);
        int d = odejmij(c, a);
        float e = dziel(c, d);
        System.out.println("Wynik dodawania "+a+" i "+b+" wynosi "+c);
        System.out.println(d);
        System.out.println(e);
    }

}
