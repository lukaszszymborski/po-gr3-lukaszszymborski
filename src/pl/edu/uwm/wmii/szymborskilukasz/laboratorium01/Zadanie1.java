package pl.edu.uwm.wmii.szymborskilukasz.laboratorium01;

public class Zadanie1 {
    static int suma(int n) {
        int wynik = 0;
        for(int i=0;i<n;i++)
        {
            wynik += i;
        }
        return(wynik);
    }
    static int moduly(int n) {
        int wynik = 1;
        for(int i=1;i<=n;i++) {
            wynik *= Math.abs(i);
        }
        return(wynik);
    }
    static int zmienne(int n) {
        int wynik = 0;
        for (int i=1;i<=n;i++)
        {
            wynik = wynik + ((int)Math.pow(-1,i+1)*i);
        }
        return(wynik);
    }
    public static void main(String[] args) {
        System.out.println(suma(5));
        System.out.println(moduly(5));
        System.out.println(zmienne(2));
    }
}
