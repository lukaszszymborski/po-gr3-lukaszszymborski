package pl.edu.uwm.wmii.szymborskilukasz.laboratorium01;
import java.util.*;

public class Zadanie2 {
    static List<Integer> wynik = new ArrayList<Integer>();
    static void podzielne3nie5(int n) {
        for(int i=0;i<n;i++) {
            if(i%3==0 && i%5!=0) {
                wynik.add(i);
            }
        }
    }
    static List<Integer> wynik2 = new ArrayList<Integer>();
    static void d(int n) {
        for(int i=1;i<=n;i++) {
            if(i<((i-1)+(i+1))/2) {
                wynik2.add(i);
            }
        }
    }
    static List<Integer> wynik3 = new ArrayList<Integer>();
    static void f(int n) {
        for(int i=1;i<=n;i++) {
            if(i%2!=0 && i%2==0) {
                wynik3.add(i);
            }
        }
    }


    public static void main(String[] args) {
        podzielne3nie5(50);
        System.out.println(wynik.size());
        d(50);
        System.out.println(wynik2.size());
        f(50);
        System.out.println(wynik3.size());
    }
}
