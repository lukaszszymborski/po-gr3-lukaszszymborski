package pl.edu.uwm.wmii.szymborskilukasz.laboratorium01;
import java.lang.*;
import java.util.*;

public class test {

    public static void main(String args[]) {
        Samochod samochod1 = new Samochod("Audi", 1990, 1.6F, 200);
        Samochod samochod2 = new Samochod("Toyota", 2000, 2, 300);
        Samochod samochod3 = new Samochod("Fiat", 1972, 1, 20);
        List<String> lista = new ArrayList<String>();
        for(int i=1; i<=Samochod.iloscObiektow; i++){
            lista.add("samochod"+i);
        }

        System.out.println(samochod1.marka);
        System.out.println(samochod2.spalanieNaTrasie);
        System.out.println(samochod3.rocznik);
        System.out.println(Samochod.iloscObiektow);
        System.out.println(lista.get(0));

    }

}
