package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.math.BigDecimal;
import java.util.Scanner;

import static java.lang.Math.pow;

public class Zadanie5 {

    static private BigDecimal kapital(int k, double p, int n) { return BigDecimal.valueOf(k).multiply(BigDecimal.valueOf(pow((int)(1000+p*10), n)/pow(1000, n))); }

    public static void main(String args[]) {
        Scanner kTemp = new Scanner(System.in);
        int k = kTemp.nextInt();
        Scanner pTemp = new Scanner(System.in);
        double p = pTemp.nextDouble();
        Scanner nTemp = new Scanner(System.in);
        int n = nTemp.nextInt();
        BigDecimal kapital = kapital(k, p, n);
        System.out.println(kapital.toString());
    }
}
