package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.util.Scanner;

public class Zadanie1b {
    static private int countSubStr(String str, String subStr) {
        char[] tekst = str.toCharArray();
        char[] podtekst = subStr.toCharArray();
        int ilosc = 0;
        for(int i=0;i<tekst.length;i++) {
            if(tekst[i]==podtekst[0]) {
                for(int j=0;j<podtekst.length;j++) {
                    if(i+j>=tekst.length) { break; }
                    if(tekst[i+j]!=podtekst[j]) {
                        continue;
                    }
                    else if(j==podtekst.length-1){
                        ilosc++;
                    }
                }
            }
        }
        return ilosc;
    }

    public static void main(String args[]) {
        Scanner tekstTemp = new Scanner(System.in);
        String tekst = tekstTemp.nextLine();
        Scanner tekstTemp2 = new Scanner(System.in);
        String substr = tekstTemp2.nextLine();
        System.out.println(countSubStr(tekst, substr));
    }
}
