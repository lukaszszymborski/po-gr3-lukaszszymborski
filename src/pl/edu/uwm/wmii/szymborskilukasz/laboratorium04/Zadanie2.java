package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.io.*;
import java.util.*;

public class Zadanie2 {
    static int countChar(String filename, char c) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        List<String> lines = new ArrayList<String>();
        String line = null;
        int ilosc = 0;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }

            bufferedReader.close();
            for (int i = 0; i < lines.size(); i++) {
                String temp = lines.get(i).toLowerCase();
                char[] tempArray = temp.toCharArray();
                for (char ch : tempArray) {
                    c = Character.toLowerCase(c);
                    if (ch == c) {
                        ilosc++;
                    }
                }
            }
            return ilosc;
        } catch (FileNotFoundException e) {
            System.err.println("Error: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }finally {
            bufferedReader.close();
        }
        return ilosc;
    }


}
