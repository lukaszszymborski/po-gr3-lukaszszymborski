package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.util.Scanner;

public class Zadanie1d {
    static private String repeat(String str, int n) {
        String[] tekst = new String[n];
        for(int i=0;i<n;i++) { tekst[i] = str; }
        return String.join("", tekst);
    }

    public static void main(String args[]) {
        Scanner tekstTemp = new Scanner(System.in);
        String tekst = tekstTemp.nextLine();
        Scanner iloscTemp = new Scanner(System.in);
        int ilosc = iloscTemp.nextInt();
        System.out.println(repeat(tekst, ilosc));
    }
}
