package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.io.IOException;
import java.util.Scanner;

public class TestZadanie3 {
    public static void main(String args[]) {

        Zadanie3 rf = new Zadanie3();

        Scanner filenameTemp = new Scanner(System.in);
        String filename = filenameTemp.nextLine();
        Scanner substrTemp = new Scanner(System.in);
        String substr = substrTemp.nextLine();
        String dir = System.getProperty("user.dir");
        try {
            int ile = Zadanie3.countWord(dir+"/src/pl/edu/uwm/wmii/szymborskilukasz/laboratorium04/"+filename+".txt", substr);
            System.out.println(ile);
        }
        catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
