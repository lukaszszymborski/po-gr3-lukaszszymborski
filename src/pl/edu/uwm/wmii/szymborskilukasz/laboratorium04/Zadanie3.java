package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.io.*;
import java.util.*;

public class Zadanie3 {
    static int countWord(String filename, String substr) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        List<String> lines = new ArrayList<String>();
        String line = null;
        char[] tempSubstr = substr.toLowerCase().toCharArray();
        int ilosc = 0;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }

            bufferedReader.close();
            for (int i = 0; i < lines.size(); i++) {
                String temp = lines.get(i).toLowerCase();
                char[] tempArray = temp.toCharArray();
                for(int k=0;k<tempArray.length;k++) {
                    if (tempArray[k] == tempSubstr[0]) {
                        for (int j = 0; j < tempSubstr.length; j++) {
                            if (k + j >= tempArray.length) {
                                break;
                            }
                            if (tempArray[k + j] != tempSubstr[j]) {
                                continue;
                            } else if (j == tempSubstr.length - 1) {
                                ilosc++;
                            }
                        }
                    }
                }
            }
            return ilosc;
        } catch (FileNotFoundException e) {
            System.err.println("Error: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }finally {
            bufferedReader.close();
        }
        return ilosc;
    }


}
