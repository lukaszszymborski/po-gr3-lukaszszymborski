package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.util.Scanner;

public class Zadanie1h {
    static private String nice(String str, char separator, int n) {
        StringBuffer tekst = new StringBuffer();
        char[] tekstTemp = str.toCharArray();
        int a = tekstTemp.length%n;
        for(int i=0;i<tekstTemp.length;i++)
        {
            if(i==0)
            {
                tekst.append(tekstTemp[i]);
            }
            else if(i%n!=a)
            {
                tekst.append(tekstTemp[i]);
            }
            else
            {
                tekst.append(separator);
                tekst.append(tekstTemp[i]);
            }
        }
        return tekst.toString();
    }

    public static void main(String args[]) {
        Scanner tekstTemp = new Scanner(System.in);
        String tekst = tekstTemp.nextLine();
        Scanner sepTemp = new Scanner(System.in);
        char separator = sepTemp.next().charAt(0);
        Scanner ileTemp = new Scanner(System.in);
        int ile = tekstTemp.nextInt();
        System.out.println(nice(tekst, separator, ile));
    }
}
