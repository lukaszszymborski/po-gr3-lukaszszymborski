package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.io.IOException;
import java.util.Scanner;

public class TestZadanie2 {
    public static void main(String args[]) {

        Zadanie2 rf = new Zadanie2();

        Scanner filenameTemp = new Scanner(System.in);
        String filename = filenameTemp.nextLine();
        Scanner literaTemp = new Scanner(System.in);
        char litera = literaTemp.next().charAt(0);
        String dir = System.getProperty("user.dir");
        try {
            int ile = Zadanie2.countChar(dir+"/src/pl/edu/uwm/wmii/szymborskilukasz/laboratorium04/"+filename+".txt", litera);
            System.out.println(ile);
        }
        catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
