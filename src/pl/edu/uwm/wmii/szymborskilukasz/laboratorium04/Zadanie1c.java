package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.util.Scanner;

public class Zadanie1c {
    static private String middle(String str) {
        char[] tekst = str.toCharArray();
        int srodek, srodek1, srodek2;
        if(tekst.length%2!=0)
        {
            srodek = (int)Math.floor((double)tekst.length/2);
            String s = Character.toString(tekst[srodek]);
            return s;
        }
        else
        {
            srodek1 = (int)Math.floor((double)tekst.length/2);
            srodek2 = (int)Math.ceil((double)tekst.length/2);
            char[] temp = new char[2];
            temp[0] = tekst[srodek1];
            temp[1] = tekst[srodek2];
            String s = String.valueOf(temp);
            return s;
        }
    }

    public static void main(String args[]) {
        Scanner tekstTemp = new Scanner(System.in);
        String tekst = tekstTemp.nextLine();
        System.out.println(middle(tekst));
    }
}
