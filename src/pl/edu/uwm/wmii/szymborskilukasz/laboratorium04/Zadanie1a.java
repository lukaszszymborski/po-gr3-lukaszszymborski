package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.util.Scanner;

public class Zadanie1a {
    static private int countChar(String str, char c) {
        char[] tekst = str.toCharArray();
        int ilosc = 0;
        for(char ch : tekst) {
            if(ch==c)
            {
                ilosc++;
            }
        }
        return ilosc;
    }

    public static void main(String args[]) {
        Scanner tekstTemp = new Scanner(System.in);
        String tekst = tekstTemp.nextLine();
        Scanner literaTemp = new Scanner(System.in);
        char litera = literaTemp.next().charAt(0);
        System.out.println(countChar(tekst, litera));
    }
}
