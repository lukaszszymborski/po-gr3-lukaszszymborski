package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.util.*;

public class Zadanie1e {
    static private int[] where(String str, String subStr) {
        char[] tekst = str.toCharArray();
        char[] podtekst = subStr.toCharArray();
        List<Integer> tempInd = new ArrayList<Integer>();
        for(int i=0;i<tekst.length;i++) {
            if(tekst[i]==podtekst[0]) {
                for(int j=0;j<podtekst.length;j++) {
                    if(i+j>=tekst.length) { break; }
                    if(tekst[i+j]!=podtekst[j]) {
                        continue;
                    }
                    else if(j==podtekst.length-1){
                        tempInd.add(i);
                    }
                }
            }
        }
        int[] indeksy = new int[tempInd.size()];
        for(int x=0;x<indeksy.length;x++) { indeksy[x] = tempInd.get(x).intValue(); }
        return indeksy;
    }

    public static void main(String args[]) {
        Scanner tekstTemp = new Scanner(System.in);
        String tekst = tekstTemp.nextLine();
        Scanner tekstTemp2 = new Scanner(System.in);
        String substr = tekstTemp2.nextLine();
        int[] indeksy = where(tekst, substr);
        for(int i=0;i<indeksy.length;i++) { System.out.print(indeksy[i]+", "); }
    }
}
