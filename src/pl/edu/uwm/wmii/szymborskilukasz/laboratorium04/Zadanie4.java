package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.math.BigInteger;
import java.util.Scanner;

public class Zadanie4 {

    static BigInteger ileZiaren(int n) {
        int tempN = n*n;
        BigInteger ziarna = new BigInteger("1");
        for(int i=1;i<=tempN;i++) {
            ziarna = ziarna.multiply(BigInteger.valueOf(2));
        }
        ziarna = ziarna.subtract(BigInteger.valueOf(1));
        return ziarna;
    }

    public static void main(String args[]) {
        Scanner nTemp = new Scanner(System.in);
        int n = nTemp.nextInt();
        BigInteger ziarna = ileZiaren(n);
        System.out.println(ziarna.toString());
    }
}
