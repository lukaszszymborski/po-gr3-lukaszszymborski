package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.util.Scanner;

public class Zadanie1f {
    static private String change(String str) {
        StringBuffer tekst = new StringBuffer(str);
        StringBuffer zamienione = new StringBuffer();
        for(int i=0;i<tekst.length();i++)
        {
            char c = tekst.charAt(i);
            if(Character.isLowerCase(c))
            {
                zamienione.append(String.valueOf(c).toUpperCase());
            }
            else
            {
                zamienione.append(String.valueOf(c).toLowerCase());
            }
        }
        return zamienione.toString();
    }

    public static void main(String args[]) {
        Scanner tekstTemp = new Scanner(System.in);
        String tekst = tekstTemp.nextLine();
        System.out.println(change(tekst));
    }
}
