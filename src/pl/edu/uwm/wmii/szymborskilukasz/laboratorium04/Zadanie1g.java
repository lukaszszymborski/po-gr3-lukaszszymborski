package pl.edu.uwm.wmii.szymborskilukasz.laboratorium04;

import java.util.Scanner;

public class Zadanie1g {
    static private String nice(String str) {
        StringBuffer tekst = new StringBuffer();
        char[] tekstTemp = str.toCharArray();
        int a = tekstTemp.length%3;
        for(int i=0;i<tekstTemp.length;i++)
        {
            if(i==0)
            {
                tekst.append(tekstTemp[i]);
            }
            else if(i%3!=a)
            {
                tekst.append(tekstTemp[i]);
            }
            else
            {
                tekst.append("'");
                tekst.append(tekstTemp[i]);
            }
        }
        return tekst.toString();
    }

    public static void main(String args[]) {
        Scanner tekstTemp = new Scanner(System.in);
        String tekst = tekstTemp.nextLine();
        System.out.println(nice(tekst));
    }
}
