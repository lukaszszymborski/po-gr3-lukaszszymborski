package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.*;

public class Zadanie1a {


    public static void main(String[] args) {
        int ile = 0;
        List<Integer> nieparzyste = new ArrayList<Integer>();
        List<Integer> parzyste = new ArrayList<Integer>();
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        if(ile>100 || ile<1)
        {
            System.out.println("Wielkość poza zakresem zadania.");
            System.exit(0);
        }
        int[] A = new int[ile];
        Random generator = new Random();
        for(int i=0;i<ile;i++) {
            A[i] = generator.nextInt(1999)-999;
        }
        for(int i=0;i<ile;i++) {
            if(A[i]%2==0)
            {
                parzyste.add(A[i]);
            }
            else
            {
                nieparzyste.add(A[i]);
            }
        }
        System.out.println("Nieparzystych jest "+nieparzyste.size());
        System.out.println("Parzystych jest "+parzyste.size());


    }
}
