package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2c {
    public static int ileMaksymalnych(int[] tab){
        int iloscmax = 0;
        int max = tab[0];
        for(int i=1;i < tab.length;i++){
            if(tab[i] > max){
                max = tab[i];
            }
        }
        for(int j=0;j<tab.length;j++) {
            if(tab[j]==max)
            {
                iloscmax++;
            }
        }
        return iloscmax;
    }


    public static void main(String[] args) {
        int ile = 0;
        int iloscmax = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        testZakresu.sprawdzZakres(ile);
        int[] A = new int[ile];
        generator.generuj(A, ile);
        int max = ileMaksymalnych(A);

        System.out.println("Największa wartość występuje "+max+" razy.");


    }
}
