package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.*;

public class Zadanie1b {


    public static void main(String[] args) {
        int ile = 0;
        List<Integer> ujemne = new ArrayList<Integer>();
        List<Integer> dodatnie = new ArrayList<Integer>();
        List<Integer> zera = new ArrayList<Integer>();
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        if(ile>100 || ile<1)
        {
            System.out.println("Wielkość poza zakresem zadania.");
            System.exit(0);
        }
        int[] A = new int[ile];
        Random generator = new Random();
        for(int i=0;i<ile;i++) {
            A[i] = generator.nextInt(1999)-999;
        }
        for(int i=0;i<ile;i++) {
            if(A[i]<0)
            {
                ujemne.add(A[i]);
            }
            else if(A[i]>0)
            {
                dodatnie.add(A[i]);
            }
            else
            {
                zera.add(A[i]);
            }
        }
        System.out.println("Ujemnych jest "+ujemne.size());
        System.out.println("Dodatnich jest "+dodatnie.size());
        System.out.println("Zer jest "+zera.size());


    }
}
