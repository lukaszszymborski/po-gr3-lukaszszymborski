package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.*;

public class Zadanie1c {
    public static int getMax(int[] tablica){
        int max = tablica[0];
        for(int i=1;i < tablica.length;i++){
            if(tablica[i] > max){
                max = tablica[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int ile = 0;
        int iloscmax = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        if(ile>100 || ile<1)
        {
            System.out.println("Wielkość poza zakresem zadania.");
            System.exit(0);
        }
        int[] A = new int[ile];
        Random generator = new Random();
        for(int i=0;i<ile;i++) {
            A[i] = generator.nextInt(1999)-999;
        }
        int max = getMax(A);
        for(int i=0;i<ile;i++) {
            if(A[i]==max)
            {
                iloscmax++;
            }
        }
        System.out.println("Największą jest "+max+" i występuje "+iloscmax+" razy.");


    }
}
