package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.Scanner;

public class Zadanie3 {
    public static int[][] pomnozMacierze(int[][] tab1, int[][] tab2) {
        int[][] pomnozona = new int[tab1.length][tab2[0].length];
        if (tab1[0].length == tab2.length) {
            //wiersze tab1
            for (int i = 0; i < tab1.length; i++) {
                //kolumny tab2
                for (int j = 0; j < tab2[0].length; j++) {
                    int temp = 0;
                    //wiersze tab2
                    for (int w = 0; w < tab2.length; w++) {
                        temp += tab1[i][w] * tab2[w][j];
                    }
                    pomnozona[i][j] = temp;
                }
            }
        }
        else
        {
            System.out.println("Podane tablice mają niewłasciwe wymiary");
            System.exit(0);
        }
        return pomnozona;
    }

    public static void main(String[] args) {
        int m = 0;
        Scanner mS = new Scanner(System.in);
        m = mS.nextInt();
        int n = 0;
        Scanner nS = new Scanner(System.in);
        n = nS.nextInt();
        int k = 0;
        Scanner kS = new Scanner(System.in);
        k = kS.nextInt();
        int[][] A = new int [m][n];
        int[][] B = new int [n][k];
        generator.generujMacierz(A);
        generator.generujMacierz(B);
        wyswietlanie.wyswietl(A);
        wyswietlanie.wyswietl(B);
        int[][] C = new int [m][k];
        C = pomnozMacierze(A, B);
        wyswietlanie.wyswietl(C);
    }
}
