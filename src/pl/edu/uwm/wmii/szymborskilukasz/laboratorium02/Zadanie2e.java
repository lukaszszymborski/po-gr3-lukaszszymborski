package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.*;

public class Zadanie2e {
    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab) {
        List<Integer> dlugosci = new ArrayList<Integer>();
        int iloscmax = 0;
        for(int i=0;i<tab.length;i++) {
            if(tab[i]>0)
            {
                iloscmax++;
                if(i==tab.length-1)
                {
                    dlugosci.add(iloscmax);
                }
            }
            else
            {
                dlugosci.add(iloscmax);
                iloscmax = 0;
            }
        }
        return Collections.max(dlugosci);
    }

    public static void main(String[] args) {
        int ile = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        //sprawdzenie zakresu
        testZakresu.sprawdzZakres(ile);
        int[] A = new int[ile];
        generator.generuj(A, ile);
        int maxdl = dlugoscMaksymalnegoCiaguDodatnich(A);
        System.out.println(maxdl);
        for(int i=0;i<ile;i++)
        {
            System.out.print(A[i]+", ");
        }


    }
}
