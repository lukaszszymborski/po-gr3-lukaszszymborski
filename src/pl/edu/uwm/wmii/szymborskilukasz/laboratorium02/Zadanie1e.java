package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.*;

public class Zadanie1e {

    public static void main(String[] args) {
        int ile = 0;
        int iloscmax = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        if(ile>100 || ile<1)
        {
            System.out.println("Wielkość poza zakresem zadania.");
            System.exit(0);
        }
        int[] A = new int[ile];
        List<Integer> dlugosci = new ArrayList<Integer>();
        Random generator = new Random();
        for(int i=0;i<ile;i++) {
            A[i] = generator.nextInt(1999)-999;
        }
        for(int i=0;i<ile;i++) {
            if(A[i]>0)
            {
                iloscmax++;
                if(i==ile-1)
                {
                    dlugosci.add(iloscmax);
                }
            }
            else
            {
                dlugosci.add(iloscmax);
                iloscmax = 0;
            }
        }
        int maxdl = Collections.max(dlugosci);
        System.out.println(maxdl);
        for(int i=0;i<ile;i++)
        {
            System.out.print(A[i]+", ");
        }


    }
}
