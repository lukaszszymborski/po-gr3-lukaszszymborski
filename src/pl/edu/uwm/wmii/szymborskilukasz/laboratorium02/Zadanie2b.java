package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie2b {
    public static int ileDodatnich(int[] tab) {
        List<Integer> dodatnie = new ArrayList<Integer>();
        for(int i=0;i<tab.length;i++) {
            if(tab[i]>0) {
                dodatnie.add(tab[i]);
            }
        }
        return dodatnie.size();
    }

    public static int ileUjemnych(int[] tab) {
        List<Integer> ujemne = new ArrayList<Integer>();
        for(int i=0;i<tab.length;i++) {
            if(tab[i]<0) {
                ujemne.add(tab[i]);
            }
        }
        return ujemne.size();
    }

    public static int ileZerowych(int[] tab) {
        List<Integer> zera = new ArrayList<Integer>();
        for(int i=0;i<tab.length;i++) {
            if(tab[i]==0) {
                zera.add(tab[i]);
            }
        }
        return zera.size();
    }

    public static void main(String[] args) {
        int ile = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        testZakresu.sprawdzZakres(ile);
        int[] A = new int[ile];
        generator.generuj(A, ile);
        int dodatnie = ileDodatnich(A);
        int ujemne = ileUjemnych(A);
        int zera = ileZerowych(A);
        System.out.println("Dodatnich jest "+dodatnie);
        System.out.println("Ujemnych jest "+ujemne);
        System.out.println("Zer jest "+zera);
    }
}
