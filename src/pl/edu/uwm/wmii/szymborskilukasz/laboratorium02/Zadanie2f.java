package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2f {
    public static void signum(int tab[]) {
        for(int i=0;i<tab.length;i++) {
            if(tab[i]>0)
            {
                tab[i] = 1;
            }
            else if(tab[i]<0)
            {
                tab[i] = -1;
            }
        }
    }

    public static void main(String[] args) {
        int ile = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        testZakresu.sprawdzZakres(ile);
        int[] A = new int[ile];
        generator.generuj(A, ile);
        signum(A);
        for(int i=0;i<ile;i++)
        {
            System.out.print(A[i]+", ");
        }


    }
}
