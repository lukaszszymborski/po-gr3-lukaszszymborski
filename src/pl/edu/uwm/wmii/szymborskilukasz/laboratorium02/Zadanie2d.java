package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2d {
    public static int sumaDodatnich(int[] tab) {
        List<Integer> dodatnie = new ArrayList<Integer>();
        int wynikd = 0;
        for(int i=0;i<tab.length;i++) {
            if (tab[i] > 0) {
                dodatnie.add(tab[i]);
            }
        }
        for(int i=0;i<dodatnie.size();i++)
        {
            wynikd += dodatnie.get(i);
        }
        return wynikd;
    }

    public static int sumaUjemnych(int[] tab) {
        List<Integer> ujemne = new ArrayList<Integer>();
        int wyniku = 0;
        for(int i=0;i<tab.length;i++) {
            if (tab[i] < 0) {
                ujemne.add(tab[i]);
            }
        }
        for(int i=0;i<ujemne.size();i++)
        {
            wyniku += ujemne.get(i);
        }
        return wyniku;
    }


    public static void main(String[] args) {
        int ile = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        testZakresu.sprawdzZakres(ile);
        int[] A = new int[ile];
        generator.generuj(A, ile);
        int wyniku = sumaUjemnych(A);
        int wynikd = sumaDodatnich(A);
        System.out.println(wyniku);
        System.out.println(wynikd);


    }
}
