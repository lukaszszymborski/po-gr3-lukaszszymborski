package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2a {
    public static int ileNieparzystych(int[] tab) {
        List<Integer> nieparzyste = new ArrayList<Integer>();
        for(int i=0;i<tab.length;i++) {
            if(tab[i]%2!=0) {
                nieparzyste.add(tab[i]);
            }
        }
        return nieparzyste.size();
    }

    public static int ileParzystych(int[] tab) {
        List<Integer> parzyste = new ArrayList<Integer>();
        for(int i=0;i<tab.length;i++) {
            if(tab[i]%2==0) {
                parzyste.add(tab[i]);
            }
        }
        return parzyste.size();
    }

    public static void main(String[] args) {
        int ile = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        testZakresu.sprawdzZakres(ile);
        int[] A = new int[ile];
        generator.generuj(A, ile);
        int nieparzyste = ileNieparzystych(A);
        int parzyste = ileParzystych(A);
        System.out.println("Nieparzystych jest "+nieparzyste);
        System.out.println("Parzystych jest "+parzyste);
    }
}
