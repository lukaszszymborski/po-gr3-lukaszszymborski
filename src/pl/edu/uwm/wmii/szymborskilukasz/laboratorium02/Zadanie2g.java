package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2g {
    public static void odwrocFragment(int[] tab, int lewy, int prawy) {
        while(lewy<prawy)
        {
            int temp = tab[prawy];
            tab[prawy] = tab[lewy];
            tab[lewy] = temp;
            lewy++;
            prawy--;
        }
    }

    public static void main(String[] args) {
        int ile = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        testZakresu.sprawdzZakres(ile);
        int[] A = new int[ile];
        generator.generuj(A, ile);
        for(int i=0;i<ile;i++)
        {
            System.out.print(A[i]+", ");
        }
        Scanner lewyS = new Scanner(System.in);
        int lewy = lewyS.nextInt()-1;
        Scanner prawyS = new Scanner(System.in);
        int prawy = prawyS.nextInt()-1;
        odwrocFragment(A, lewy, prawy);
        for(int i=0;i<ile;i++)
        {
            System.out.print(A[i]+", ");
        }
    }
}
