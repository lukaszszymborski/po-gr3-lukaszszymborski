package pl.edu.uwm.wmii.szymborskilukasz.laboratorium02;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1g {

    public static void main(String[] args) {
        int ile = 0;
        Scanner ilosc = new Scanner(System.in);
        ile = ilosc.nextInt();
        if(ile>100 || ile<1)
        {
            System.out.println("Wielkość poza zakresem zadania.");
            System.exit(0);
        }
        int[] A = new int[ile];
        Random generator = new Random();
        for(int i=0;i<ile;i++) {
            A[i] = generator.nextInt(1999)-999;
        }
        for(int i=0;i<ile;i++)
        {
            System.out.print(A[i]+", ");
        }
        Scanner lewyS = new Scanner(System.in);
        int lewy = lewyS.nextInt()-1;
        Scanner prawyS = new Scanner(System.in);
        int prawy = prawyS.nextInt()-1;
        while(lewy<prawy)
        {
            int temp = A[prawy];
            A[prawy] = A[lewy];
            A[lewy] = temp;
            lewy++;
            prawy--;
        }
        for(int i=0;i<ile;i++)
        {
            System.out.print(A[i]+", ");
        }


    }
}
